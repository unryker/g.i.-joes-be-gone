### Notes
---
- This is a Vanilla-friendly fork of [JoeyTD/BeardedDoomguy's GZDoom-only mod](https://forum.zdoom.org/viewtopic.php?f=46&t=73890).
- Replaces the hanging body sprites and textures introduced in the first Doom with graphics from PSX Doom that is more suitable for the art style.
	- The sprite replacements are done through DEHACKED string renaming.

### Build instructions
---
- [Build the repository with DeuTex.](https://github.com/Doom-Utils/deutex) For example, to build with Windows PowerShell, enter `./deutex -doom2 c:/doom/ -s_end -build gijoesinfo.txt GIJoesBeGone.wad`. The outputted .WAD file works out of the box with Chocolate Doom (using -dehlump), PrBoom+ and GZDoom.
- To get the loose DEHACKED file for Vanilla Doom, look for `lumps/dehacked.lmp` and rename the `dehacked.lmp` to `GIJoesBeGone.deh`.

### How to run
---
- Chocolate Doom / Crispy Doom
	- Run with `-file GIJoesBeGone.wad -dehlump GIJoesBeGone.wad`. Or do `-file GIJoesBeGone.wad -deh GIJoesBeGone.deh`.
- Other source ports (GZDoom, PrBoom)
	- Only load `GIJoesBeGone.wad`. 9/10 source ports will automatically load in the DEHACKED that's built in.

### Compatibility
---
- [Doom Minor Spritefixing Project](https://www.doomworld.com/forum/topic/62403-doom-2-minor-sprite-fixing-project-v20-beta-1-release-updated-11822/)
	- Load G.I. Joes Be Gone AFTER Doom Minor Spritefixing Project. The wrong order will result in the G.I. Joe bodies being thematically unchanged.
	- Note that this contains sprite-padding fixes and extended chains/ropes exactly like Spritefixing Project.
- [Undead Doom Patch](https://gitlab.com/u344/undead-doom)
	- Only load Undead Doom Patch. G.I. Joes Be Gone isn't needed, as Undead Doom Patch comes with it already bundled.